// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TestAssignmentGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TESTASSIGNMENT_API ATestAssignmentGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TestAssignmentPlayerController.generated.h"


namespace PlayFab { class UPlayFabClientAPI; }
namespace PlayFab { namespace ClientModels { struct FLoginResult; } }
namespace PlayFab { struct FPlayFabCppError; }
namespace PlayFab { namespace ClientModels { struct FRegisterPlayFabUserResult; } }

DECLARE_LOG_CATEGORY_EXTERN(LogPlayFab, Verbose, All);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPFCppError, FString, ErrorMessage);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPFRegistrationSuccess);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPFRegistrationFailed);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnPFLoginSuccess, FDateTime, RegistrationTime, FDateTime, LastLoginTime, int32, LoginCount, bool, bNewUser);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPFLoginFailed);

/**
 * 
 */
UCLASS()
class TESTASSIGNMENT_API ATestAssignmentPlayerController : public APlayerController
{
	GENERATED_BODY()
	
protected:
	TSharedPtr<class PlayFab::UPlayFabClientAPI> clientAPI = nullptr;

public:
	UPROPERTY(BlueprintAssignable)
	FOnPFRegistrationSuccess OnPFRegistrationSuccess;

	UPROPERTY(BlueprintAssignable)
	FOnPFRegistrationFailed OnPFRegistrationFailed;

	UPROPERTY(BlueprintAssignable)
	FOnPFLoginSuccess OnPFLoginSuccess;

	UPROPERTY(BlueprintAssignable)
	FOnPFLoginFailed OnPFLoginFailed;

	UPROPERTY(BlueprintAssignable)
	FOnPFCppError OnPFCppError;

public:
	virtual void PostInitializeComponents() override;

protected:
	virtual void PF_OnCppError(const PlayFab::FPlayFabCppError& ErrorResult) const;

	virtual void PF_OnRegistrationSuccess(const PlayFab::ClientModels::FRegisterPlayFabUserResult& RegisterResult) const;
	virtual void PF_OnRegistrationFailed(const PlayFab::FPlayFabCppError& ErrorResult) const;
	
	virtual void PF_OnLoginSuccess(const PlayFab::ClientModels::FLoginResult& Result) const;
	virtual void PF_OnLoginFailed(const PlayFab::FPlayFabCppError& ErrorResult) const;

	virtual void PF_UpdateLoginsCount(const int32& InLoginsCount) const;

public:
	UFUNCTION(BlueprintCallable)
	virtual void PF_TryRegister(FString Login, FString Password) const;

	UFUNCTION(BlueprintCallable)
	virtual void PF_TryLogin(FString Login, FString Password) const;
};


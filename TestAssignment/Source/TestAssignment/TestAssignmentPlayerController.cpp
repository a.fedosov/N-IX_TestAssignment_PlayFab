// Fill out your copyright notice in the Description page of Project Settings.

#include "TestAssignmentPlayerController.h"
#include "PlayFab.h"
#include "Core/PlayFabError.h"
#include "Core/PlayFabClientDataModels.h"
#include "PlayFabCpp/Public/Core/PlayFabClientAPI.h"

void ATestAssignmentPlayerController::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	clientAPI = IPlayFabModuleInterface::Get().GetClientAPI();
}

void ATestAssignmentPlayerController::PF_OnCppError(const PlayFab::FPlayFabCppError& ErrorResult) const
{
	UE_LOG(LogPlayFab, Error, TEXT("Smth went wrong... Error: %s"), *ErrorResult.GenerateErrorReport());
	OnPFCppError.Broadcast(*ErrorResult.GenerateErrorReport());
}

void ATestAssignmentPlayerController::PF_OnRegistrationSuccess(const PlayFab::ClientModels::FRegisterPlayFabUserResult& RegisterResult) const
{
	OnPFRegistrationSuccess.Broadcast();
}

void ATestAssignmentPlayerController::PF_OnRegistrationFailed(const PlayFab::FPlayFabCppError& ErrorResult) const
{
	PF_OnCppError(ErrorResult);
	OnPFRegistrationFailed.Broadcast();
}

void ATestAssignmentPlayerController::PF_TryLogin(FString Login, FString Password) const
{
	PlayFab::ClientModels::FLoginWithEmailAddressRequest request;
	request.Email = Login;
	request.Password = Password;
	request.InfoRequestParameters = MakeShareable(new PlayFab::ClientModels::FGetPlayerCombinedInfoRequestParams());
	request.InfoRequestParameters->GetPlayerProfile = true;
	request.InfoRequestParameters->GetUserAccountInfo = true;
	request.InfoRequestParameters->GetPlayerStatistics = true;

	clientAPI->LoginWithEmailAddress(request,
		PlayFab::UPlayFabClientAPI::FLoginWithEmailAddressDelegate::CreateUObject(this, &ATestAssignmentPlayerController::PF_OnLoginSuccess),
		PlayFab::FPlayFabErrorDelegate::CreateUObject(this, &ATestAssignmentPlayerController::PF_OnLoginFailed)
	);
}

void ATestAssignmentPlayerController::PF_TryRegister(FString Login, FString Password) const
{
	PlayFab::ClientModels::FRegisterPlayFabUserRequest request;
	request.Email = Login;
	request.Password = Password;
	request.RequireBothUsernameAndEmail = false;

	clientAPI->RegisterPlayFabUser(request,
		PlayFab::UPlayFabClientAPI::FRegisterPlayFabUserDelegate::CreateUObject(this, &ATestAssignmentPlayerController::PF_OnRegistrationSuccess),
		PlayFab::FPlayFabErrorDelegate::CreateUObject(this, &ATestAssignmentPlayerController::PF_OnRegistrationFailed)
	);
}

void ATestAssignmentPlayerController::PF_OnLoginSuccess(const PlayFab::ClientModels::FLoginResult& Result) const
{
	PlayFab::ClientModels::FLoginResult Test = Result;
	UE_LOG(LogPlayFab, Display, TEXT("Logged in"));

	int32 LoginsCount = 0;
	for (const PlayFab::ClientModels::FStatisticValue& PlayerStatistic : Result.InfoResultPayload->PlayerStatistics)
	{
		if (PlayerStatistic.StatisticName == FString("Logins"))
		{
			LoginsCount = PlayerStatistic.Value;
		}
	}

	OnPFLoginSuccess.Broadcast(Result.InfoResultPayload->AccountInfo->Created, Result.LastLoginTime, ++LoginsCount, Result.NewlyCreated);
	
	PF_UpdateLoginsCount(LoginsCount);
}

void ATestAssignmentPlayerController::PF_OnLoginFailed(const PlayFab::FPlayFabCppError& ErrorResult) const
{
	PF_OnCppError(ErrorResult);
	OnPFLoginFailed.Broadcast();
}

void ATestAssignmentPlayerController::PF_UpdateLoginsCount(const int32& InLoginsCount) const
{
	PlayFab::ClientModels::FStatisticUpdate LoginsStatistic;
	LoginsStatistic.StatisticName = FString("Logins");
	LoginsStatistic.Value = InLoginsCount;

	PlayFab::ClientModels::FUpdatePlayerStatisticsRequest request;
	request.Statistics.Add(LoginsStatistic);

	clientAPI->UpdatePlayerStatistics(request,
		PlayFab::UPlayFabClientAPI::FUpdatePlayerStatisticsDelegate(),
		PlayFab::FPlayFabErrorDelegate::CreateUObject(this, &ATestAssignmentPlayerController::PF_OnCppError));
}

